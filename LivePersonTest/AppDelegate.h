//
//  AppDelegate.h
//  LivePersonTest
//
//  Created by Ezequiel Becerra on 7/13/15.
//  Copyright (c) 2015 Betzerra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


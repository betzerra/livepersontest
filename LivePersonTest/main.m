//
//  main.m
//  LivePersonTest
//
//  Created by Ezequiel Becerra on 7/13/15.
//  Copyright (c) 2015 Betzerra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
